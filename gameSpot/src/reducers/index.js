import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import BasketReducer from './BasketReducer';
import ProductReducer from './ProductReducer';

export default combineReducers({
    auth: AuthReducer,
    basket: BasketReducer,
    products: ProductReducer
});
