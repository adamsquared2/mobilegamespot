import { CHECKOUT } from '../actions';

const INITIAL_STATE = {
  total: 0
}

export default(state = INITIAL_STATE, action) => {
  switch(action.type) {
    case CHECKOUT:
      return { ...state, total: action.payload };
    default: 
      return state;
  }
}