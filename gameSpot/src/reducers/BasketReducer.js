import { ADD_ITEM, FETCH_BASKET } from "../actions/types";

const INITIAL_STATE = {
  basket: []
};

export default(state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ADD_ITEM: {
      const newBasket = [...state.basket, action.payload];
      return {...state, basket: newBasket}
    }
    case FETCH_BASKET:
      return {...state, basket: action.payload};
    default: 
      return state;
  }
};
