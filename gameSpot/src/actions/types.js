//Auth Types
export const EMAIL_CHANGED = 'email_changed';
export const PASSWORD_CHANGED = 'password_changed';
export const LOGIN_USER_SUCCESS = 'login_user_success';
export const LOGIN_USER_FAIL = 'login_user_fail';
export const LOGIN_USER = 'login_user';

//Basket Types
export const ADD_ITEM = 'add_item';
export const REMOVE_ITEM = 'remove_item';
export const FETCH_BASKET = 'fetch_basket';
export const CHECKOUT = 'checkout';

//Product Types
export const FETCH_PRODUCTS = 'fetch_products';
export const MINUS_STOCK = 'minus_stock';
