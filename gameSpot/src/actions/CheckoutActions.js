import { Actions } from 'react-native-router-flux';

import { CHECKOUT } from './types';

const checkoutItems = (dispatch, total) => {
  dispatch({
      type: CHECKOUT,
      payload: total
  });
  Actions.checkout();
};
