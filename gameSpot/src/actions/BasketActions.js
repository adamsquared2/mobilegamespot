import firebase from 'firebase';
import { Actions, Reducer } from 'react-native-router-flux';

import {
    ADD_ITEM,
    REMOVE_ITEM,
    FETCH_BASKET
} from './types';

export const addItem = (item) => {
    const { currentUser } = firebase.auth();

    return (dispatch) => {
        firebase.database().ref(`/basket/${currentUser.uid}/`)
            .push(item);

        minusStock(item);
        dispatch({
            type: ADD_ITEM,
            payload: item
        });
    };
};

export const removeItem = (product) => {
    const { currentUser } = firebase.auth();

    return (dispatch) => {
        let basket = [];
        let removed = false;

        let ref = firebase.database().ref(`/basket/${currentUser.uid}`);
        ref.on('value', snapshot => {

            snapshot.forEach(x => {
                basket.push(x.val());
            })
        })

        ref.remove();

        basket.map(item => {
            if (item.Code === product.Code && !removed) {
                removed = true;
            } else {
                ref.push(item);
            };
        })
        plusStock(product);
        dispatch({ type: REMOVE_ITEM, payload: product });
    };
};



minusStock = (product) => {
    let products = [];
    const ref = firebase.database().ref(`/products`);


    ref.on("value", snapshot => {
        snapshot.forEach(x => {
            products.push(x.val());
        })
    })
    ref.remove();

    products.map(item => {
        if (item.Code === product.Code) {
            item.InStock -= 1;
        } 
        ref.push(item);
    })
}

plusStock = (product) => {
    let products = [];
    const ref = firebase.database().ref(`/products`);


    ref.on("value", snapshot => {
        snapshot.forEach(x => {
            products.push(x.val());
        })
    })
    ref.remove();

    products.map(item => {
        if (item.Code === product.Code) {
            item.InStock += 1;
        } 
        ref.push(item);
    })
}

export const fetchBasket = () => {
    const { currentUser } = firebase.auth();

    return (dispatch) => {
        firebase.database().ref(`/basket/${currentUser.uid}`)
            .on("value", snapshot => {
                let basket = [];
                snapshot.forEach(x => {
                    basket.push(x.val());
                })
                dispatch({ type: FETCH_BASKET, payload: basket });
            });
    };
};