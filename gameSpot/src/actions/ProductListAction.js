import firebase from 'firebase';
import { FETCH_PRODUCTS, MINUS_STOCK } from './types';

export const fetchProducts = () => {

  return (dispatch) => {
    firebase.database().ref(`/products`)
      .on("value", snapshot => {
        let products = [];
        snapshot.forEach(x => {
          products.push(x.val());
        })
        dispatch({ type: FETCH_PRODUCTS, payload: products });
      });
  };
};
