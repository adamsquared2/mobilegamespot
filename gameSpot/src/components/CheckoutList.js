import React, { Component } from 'react';
import { View, Text, Modal, Platform } from 'react-native';
import { Actions } from 'react-native-router-flux';
import firebase from 'firebase'
import { Card, Button, Header } from './common';
import PaymentForm from './PaymentForm';
import AddressForm from './AddressForm';

class CheckoutList extends Component {
  state = {
    modalVisible: false
  }

  setModalVisible() {
    const { modalVisible } = this.state
    this.setState({modalVisible: !modalVisible});
  }

  checkOutButton() {
    this.setModalVisible()
    const { currentUser } = firebase.auth();
    firebase.database().ref(`/basket/${currentUser.uid}/`).remove();
  }

  render() {
    const { textStyle } = styles;

    return (
      <View>
      <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <View style={{marginTop: Platform.OS === 'ios' ? 22: 0, flex: 1 }}>
            <View>
              <Header  headerText={'Checkout'}/>
              
              <View style={{ alignItems: 'center' }}>
                <Text style={{ fontSize: 18, marginBottom: 20, padding: 10, alignSelf: 'center' }}>Thank you for your purchase!</Text>
                <Button
                  style={{ backgroundColor: '#4CE0B3' }}
                  tStyle={{ fontWeight: 'normal' }}
                  onPress={() => {
                    this.setModalVisible();
                    Actions.main();
                  }}>
                  <Text style={{ fontSize: 18, fontWeight: '500' }}>Done</Text>
                </Button>
              </View>
            </View>
          </View>
        </Modal>

        <Text style={textStyle}>{`Total £${this.props.total.toFixed(2)}`}</Text>
        <Card>
          <PaymentForm/>
        </Card>
        <Card>
          <AddressForm/>
        </Card>
        <Button style={{ margin: 10, backgroundColor: '#4CE0B3' }} onPress={() => this.checkOutButton()}>
          Buy
        </Button>
      </View>
    );
  }
}

const styles = {
  textStyle: {
    fontSize: 18, 
    fontWeight: '600', 
    textAlign: 'center', 
    padding: 8 
  }
}

export default CheckoutList;
