import React, { Component } from 'react';
import { View, TouchableOpacity, Modal, Image, Platform } from 'react-native';
import { Card, Content, List, ListItem, Thumbnail, Left, Body, Text, Right, Toast, Root } from 'native-base'
import { Header, Button, Mountains } from './common';
import { addItem } from '../actions';
// import { CardSection, Card } from './common';
import Chocolate from '../resources/images/chocolate.png';
import Cookies from '../resources/images/cookies.png';
import Doughnut from '../resources/images/doughnut.png';
import Sweets from '../resources/images/sweet.png';
import Coffee from '../resources/images/coffee.png';
import Tea from '../resources/images/tea-pot.png';
import CocaCola from '../resources/images/cola.png';
import Joystick from '../resources/images/joystick.png';
import Mouse from '../resources/images/mouse.png';
import connect from 'react-redux/lib/connect/connect';

class ProductItem extends Component {

  state ={
    modalVisible: false,
  }

  setModalVisible() {
    const { modalVisible } = this.state
    this.setState({modalVisible: !modalVisible});
  }


  buyItem() {
    this.props.product.InStock -= 1;
    this.props.addItem(this.props.product)
    Toast.show({
      text:"Added to basket!",
      duration: 2500,
      buttonText: "Okay",
      position: 'top'
    })
  }

  pic() {
    const { Picture } = this.props.product;

    switch (Picture) {
      case 'doughnut':
        return Doughnut;
      case 'chocolate':
        return Chocolate;
      case 'cookies':
        return Cookies;
      case 'sweet':
        return Sweets
      case 'coffee':
        return Coffee;
      case 'tea-pot':
        return Tea;
      case 'cola':
        return CocaCola;
      case 'joystick':
        return Joystick;
      case 'mouse':
        return Mouse;
      default:
        return Coffee;
    }
  }

  renderButton(){
    const { InStock } = this.props.product;
    if( InStock ) {
      return (
        <Button style={styles.addButton} tStyle={{ fontWeight: 'normal'}} onPress={() => this.buyItem()}> Add To Basket </Button>
      );
    }
    return (
      <Button disabled style={styles.outStock} tStyle={{ fontWeight: 'normal'}}> Out of Stock </Button>
    )
  }

  render() {
    console.log(this.props.product.InStock);
    
    const { Name, Description, Price, InStock } = this.props.product;

    return (
      <View>
      <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <Root>
            <View style={{marginTop: Platform.OS === 'ios' ? 22: 0, flex: 1 }}>
              <View>
                <Header  headerText={Name}/>
                
                <View style={{ alignItems: 'center' }}>
                  <Image style={{ width: 250, height: 250, marginBottom: 20 }} source={this.pic()}/>
                  <Text style={{ fontSize: 18, marginBottom: 20, padding: 10, alignSelf: 'center' }}>{Description}</Text>
                  <Text style={{ fontWeight: 'bold', marginBottom: 20 }}>Stock: {InStock}</Text>
                  {this.renderButton()}
                  <Button
                    tStyle={{ fontWeight: 'normal' }}
                    onPress={() => {
                      this.setModalVisible();
                    }}>
                    <Text>Go Back</Text>
                  </Button>
                </View>
              </View>
            </View>
          </Root>
        </Modal>

      <TouchableOpacity onPress={() => this.setModalVisible()}>
        <Card pointerEvents="none">
          <View style={styles.cardStyle}>
            <Content>
              <List>
                <ListItem thumbnail >
                  <Left>
                    <Thumbnail source={this.pic()} />
                  </Left>
                  <Body>
                    <Text >
                      {Name}
                    </Text>
                    <Text note numberOfLines={1}>
                      {Description}
                    </Text>
                  </Body>
                  <Right>
                    <Text note>
                      £{Price}
                    </Text>
                  </Right>
                </ListItem>
              </List>
            </Content>
          </View>
        </Card>
      </TouchableOpacity>
      </View>
    );
  }
}

const styles = {
  cardStyle:{
    flexDirection: 'row',
  },
  titleStyle: {
    alignSelf: 'flex-start',
    fontSize: 15,
    paddingLeft: 15,
    flexWrap: "wrap"
  },
  textStyle:{
    flexWrap: "wrap",
    flex: 2
  },
  imageStyle: {
    alignItems: 'flex-start',
    width: 100,
    height: 100,
    padding: 5,
    flex: 1
  },
  addButton:{ 
    marginBottom: Platform.OS == 'ios' ? 150 : 10,
    backgroundColor: '#4CE0B3'
  },
  outStock:{ 
    marginBottom: Platform.OS == 'ios' ? 150 : 10,
    backgroundColor: '#DD614A'
  }
};

const mapStateToProps = (state) => {
  const { products } = state.products;

  return { products };
}

export default connect(mapStateToProps, { addItem })(ProductItem);