import React, { Component } from 'react';
import { FlatList, View, Text, ScrollView } from 'react-native';
import { connect } from 'react-redux';

import { Button } from './common'
import { fetchBasket, checkoutItems } from '../actions'
import BasketItem from './BasketItem'
import { Actions } from 'react-native-router-flux';

class Basket extends Component {
  constructor(props) {
    super(props);

    this.state = {
      total: 0
    }
  }


  componentDidMount() {
    this.props.fetchBasket();
  }

  getTotal(items, price) {
    return items.reduce(function (a, b) {
      console.log(b[price])
      let x = parseFloat(a);
      let y = parseFloat(b[price])
      return x + y;
    }, 0);

  }

  onButtonPress() {
    console.log('YOOOO' + this.state.total);
  }

  render() {
    console.log(this.props);
    let total = this.getTotal(this.props.basket, 'Price');

    console.log('total', total);

    return (
      <View style={{ alignItems: 'center' }}>
        <View style={{ backgroundColor: '#F7F7F7', width: 350, height: 550, marginTop: 10 }}>
          <FlatList
            keyExtractor={(item, index) => index.toString()}
            data={this.props.basket}
            renderItem={({ item }) => <BasketItem product={item} />}
          />
        </View>
        <Text style={{ fontSize: 20, fontWeight: 'bold', marginTop: 20 }}> £ {total.toFixed(2)} </Text>
        <Button style={{ marginTop: 20 }} onPress={() => Actions.checkout({ total })}>
          Checkout
        </Button>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const { basket } = state.basket;

  return { basket };
}

export default connect(mapStateToProps, { fetchBasket, checkoutItems })(Basket);