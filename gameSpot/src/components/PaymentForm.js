import React, { Component } from 'react';
import { Card } from './common';
import { Text, View } from 'react-native';
import { Item, Label, Input, Form, CardItem } from 'native-base';

class PaymentForm extends Component {
  render() {
    const { textStyle, headerStyle } = styles;

    return (
      <View>
        <Form>
          <CardItem style={headerStyle}>
          <Text style={textStyle}>Payment Details</Text>
          </CardItem>

          <CardItem>
          <Item floatingLabel>
            <Label>Name on Card</Label>
            <Input />
          </Item>
          </CardItem>

          <CardItem>
          <Item floatingLabel>
            <Label>Card Number</Label>
            <Input />
          </Item>
          </CardItem>

          <CardItem>
          <Item floatingLabel>
            <Label>Expiry Date</Label>
            <Input />
          </Item>
          </CardItem>

          <CardItem>
          <Item floatingLabel>
            <Label>CVC Number</Label>
            <Input />
          </Item>
          </CardItem>
        </Form>
      </View>
        
    );
  }
}

const styles = {
  textStyle: {
    fontSize: 18, 
    fontWeight: '600', 
    textAlign: 'center', 
    paddingTop: 8 
  },
  headerStyle: {
    justifyContent: 'center'
  }
}

export default PaymentForm;
