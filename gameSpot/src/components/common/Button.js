import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

const Button = ({ style, tStyle, onPress, disabled, children}) => {
  const { buttonStyle, textStyle } = styles;

  return (
    <TouchableOpacity onPress={onPress} disabled={disabled} style={[buttonStyle, style]}>
      <Text style={[textStyle, tStyle]}>
        {children}
      </Text>
    </TouchableOpacity>
  );
};

const styles = {
  textStyle: {
    alignSelf: 'center',
    fontSize: 16,
    fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10,
    color: 'black'
  },
  buttonStyle: {
    backgroundColor: '#fff',
    borderRadius: 5,
    borderColor: '#007aff',
    marginLeft: 5,
    marginRight: 5,
    width: 350,
    borderWidth: 0,
    alignSelf: 'center',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    elevation: 2,
  },
  
};

export { Button };
