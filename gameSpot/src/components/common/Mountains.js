import React from 'react';

import { View } from 'react-native';

const Mountains = () => {
    return (
        <View style={{ flex: 1, zIndex: 0 }}>
            <View style={styles.blackBox} />
            <View style={styles.blackBox1} />
        </View>
    );
};

const styles = {
    blackBox: {
        position: 'absolute',
        bottom: -350,
        right: 20,
        backgroundColor: 'black',
        borderColor: 'white',
        borderWidth: 1, 
        width: 300, 
        height: 500, 
        transform: [{ rotate: '45deg' }],
      
    },
    blackBox1: {
        position: 'absolute',
        bottom: -300,
        left: 20,
        backgroundColor: 'black',
        borderColor: 'white',
        borderWidth: 1, 
        width: 300, 
        height: 500, 
        transform: [{ rotate: '-45deg' }],
        
    }
};

export { Mountains };
