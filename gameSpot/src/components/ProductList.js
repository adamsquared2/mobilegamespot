import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchProducts } from '../actions';
import { FlatList, Text, View } from 'react-native';
import ProductItem from './ProductItem';

class ProductList extends Component {

  state = {
    filteredProducts: {}
  }
  
  componentDidMount(){
    this.props.fetchProducts();
    const tempProducts = this.props.products;

    let filteredProducts = {};
    switch(this.props.category){
      case 'Games':
        filteredProducts = tempProducts.filter(item => item.Category === 'Games');
        return this.setState({
          ...this.state,
          filteredProducts
        });
      case 'Drinks':
        filteredProducts = tempProducts.filter(item => item.Category === 'Beverages');
        return this.setState({
          ...this.state,
          filteredProducts
        });
      case 'Snacks':
        filteredProducts = tempProducts.filter(item => item.Category === 'Food');
        return this.setState({
          ...this.state,
          filteredProducts
        });
      default : 
          return this.setState({
            ...this.state,
            filteredProducts: tempProducts
          });
    };
  }


  render() {

    return (
      <View style={{ marginHorizontal: 10 }}>
        <FlatList
        keyExtractor={(item, index) => index.toString()}
        data={this.state.filteredProducts}
        renderItem={({item}) => <ProductItem product={item} />}
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const { products } = state.products;

  return { products };
};

export default connect(mapStateToProps, { fetchProducts })(ProductList);