import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { connect } from 'react-redux';

import { emailChanged, passwordChanged, loginUser } from '../actions';
import { CardSection, Input, Button, Spinner, Mountains } from './common';

class LoginForm extends Component {
  onEmailChange(text) {
    this.props.emailChanged(text);
  }

  onPasswordChange(text) {
    this.props.passwordChanged(text);
  }
  
  onButtonPress() {
    const { email, password } = this.props;
    this.props.loginUser({ email, password });
  }

  renderError() {
    if (this.props.error) {
      return ( 
        <View>
          <Text style={styles.errorTextStyle}>
            { this.props.error }
          </Text>
        </View>
      );
    }
  }

  renderButton() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    } return (
        <Button 
          onPress={this.onButtonPress.bind(this)}
        >
          Login
        </Button>
      );
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View>
          <CardSection style={styles.blackTop}> 
            <Input 
              label="Email"
              placeholder="Email@gmail.com"
              onChangeText={this.onEmailChange.bind(this)}
              value={this.props.email}
            />
          </CardSection>
          <CardSection style={styles.blackBottom}>
            <Input
              secureTextEntry
              label="Password"
              placeholder="password"
              onChangeText={this.onPasswordChange.bind(this)}
              value={this.props.password}
            />
          </CardSection>
        </View>
        {this.renderError()}
        <CardSection style={styles.overRide}>
          {this.renderButton()}
        </CardSection>
        <Mountains />
      </View>
    );
  }
}

const styles = {
  errorTextStyle: {
    fontSize: 20,
    alignSelf: 'center',
    justifyContent: 'center',
    color: 'red'
  },
  blackTop: {
    marginTop: 100,
    width: 350,
    borderTopStartRadius: 10,
    borderTopEndRadius: 10,
    alignSelf: 'center',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    elevation: 2,
  },
  blackBottom: {
    width: 350,
    borderColor: 'black',
    borderBottomStartRadius: 10,
    borderBottomEndRadius: 10,
    borderBottomWidth: 0,
    borderTopWidth: 0,
    alignSelf: 'center',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    elevation: 2,
    marginBottom: 5,
  },
  overRide: { 
    borderBottomWidth: 0,
    alignSelf: 'center',
    backgroundColor: null,
  },
  
};

const mapStateToProps = state => {
  return {
    email: state.auth.email,
    password: state.auth.password,
    error: state.auth.error,
    loading: state.auth.loading
  };
};

export default connect(mapStateToProps, { emailChanged, passwordChanged, loginUser })(LoginForm);
