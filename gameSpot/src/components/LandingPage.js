import React from 'react';
import { View, Text } from 'react-native';
import { fetchProducts, fetchBasket } from '../actions';
import { Button, Mountains } from './common';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';

class LandingPage extends React.Component { 
    componentWillMount() {
        console.log('poo');
        this.props.fetchProducts();
        this.props.fetchBasket();
    }
    render() {
        const { header, body } = styles;
        
        return (
                <View style={{ flex: 1 }}>
                    <Text style={header}>About us</Text>
                    <Text style={body}>
                    We are an online shop dedicated to serving gamers around the world.
                    We stock only the finest and freshest beverages and snacks about and will have them
                    delivered to your doorstep within record times. Not only do we do snacks and the likes,
                    we also have a large selection of games and game accersories.
                    We like all gamers strive to be the best.
                    At Adamzon You'll only get the best of service.
                    </Text>
                    <Button style={{ marginTop: 20, marginBottom: 20 }} onPress={() => Actions.productList({category: 'Snacks' })}>
                        Snacks
                    </Button>
                    <Button style={{ marginBottom: 20 }} onPress={() => Actions.productList({category: 'Drinks' })}>
                        Drinks
                    </Button>
                    <Button style={{ marginBottom: 20 }} onPress={() => Actions.productList({category: 'Games' })}>
                        Games
                    </Button>
                    <Button onPress={() => Actions.productList()}>
                        All
                    </Button>

                    <Mountains />
                </View>
        );
    }
}

const styles = {
    header: {
        marginTop: 20,
        fontSize: 40,
        alignSelf: 'center',
    }, 
    body: {
        marginTop: 20,
        padding: 5,
        textAlign: 'center',
        alignSelf: 'center',
    },
};

const mapStateToProps = (state) => {
    const { products } = state.products;
    const { basket } = state.basket;
  
    return { products, basket };
  };

export default connect(mapStateToProps, { fetchProducts, fetchBasket })(LandingPage);
