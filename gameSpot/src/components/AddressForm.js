import React, { Component } from 'react';
import { Card } from './common';
import { Text, View } from 'react-native';
import { Item, Label, Input, Form, CardItem } from 'native-base';

class AddressForm extends Component {
  render() {
    const { textStyle, headerStyle } = styles;

    return (
      <View>
        <Form>
          <CardItem style={headerStyle}>
          <Text style={textStyle}>Delivery Address</Text>
          </CardItem>

          <CardItem>
          <Item floatingLabel>
            <Label>Number</Label>
            <Input />
          </Item>
          </CardItem>

          <CardItem>
          <Item floatingLabel>
            <Label>Street</Label>
            <Input />
          </Item>
          </CardItem>

          <CardItem>
          <Item floatingLabel>
            <Label>Town</Label>
            <Input />
          </Item>
          </CardItem>

          <CardItem>
          <Item floatingLabel>
            <Label>City</Label>
            <Input />
          </Item>
          </CardItem>
          
          <CardItem>
          <Item floatingLabel>
            <Label>Postcode</Label>
            <Input />
          </Item>
          </CardItem>
        </Form>
      </View>
    );
  }
}

const styles = {
  textStyle: {
    fontSize: 18, 
    fontWeight: '600', 
    textAlign: 'center', 
    paddingTop: 8 
  },
  headerStyle: {
    justifyContent: 'center'
  }
}

export default AddressForm;
