import React, { Component } from 'react';
import { View, Text, FlatList, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { Content, Form, Item, Label, Input, CardItem } from 'native-base';
import { Button, Card } from './common';
import { fetchBasket } from '../actions';
import CheckoutList from './CheckoutList';

class Checkout extends Component {
  render() {
    console.log(this.props);
    
    return (
      <ScrollView style={{ marginHorizontal: 10 }}>
      <CheckoutList total={this.props.total}/>
      </ScrollView>
    );
  }
}

const mapStateToProps = (state) => {
    const { basket } = state.basket;

    return { basket };
}

export default connect(mapStateToProps, { fetchBasket } )(Checkout);