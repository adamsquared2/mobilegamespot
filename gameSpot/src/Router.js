import React from 'react';
import { Image } from 'react-native';
import { Scene, Router, Actions } from 'react-native-router-flux';
import firebase from 'firebase';
import LoginForm from './components/LoginForm';
import LandingPage from './components/LandingPage';
import ProductList from './components/ProductList';
import Basket from './components/Basket';
import Checkout from './components/Checkout';

const RouterComponent = () => {
    const { titleStyle } = styles;
    return (
        <Router>
            <Scene key="root" hideNavBar>
                <Scene key="auth" titleStyle={titleStyle}>
                    <Scene
                        key="login"
                        renderTitle={<Image style={{ width: 84, height: 27}}source={require('./resources/images/Adamzon-Logo.png')} />}
                        component={LoginForm}
                        initial
                    />
                </Scene>
                <Scene key="main" panHandlers={null} titleStyle={titleStyle}>
                    <Scene
                        key="landingPage"
                        component={LandingPage}
                        renderTitle={<Image style={{ width: 84, height: 27, paddingBottom: 20}}source={require('./resources/images/Adamzon-Logo.png')} />}
                        initial
                        panHandlers={null}
                        onRight={() => Actions.basket()}
                        rightButtonImage={require('./resources/images/basket.png')}
                        onLeft={() => {firebase.auth().signOut(); Actions.auth()}}
                        leftTitle="Logout"
                    />
                    <Scene
                        key="productList"
                        component={ProductList}
                        title="Products"
                    />
                    <Scene
                        key='basket'
                        component={Basket}
                        title="Basket"
                    />
                    <Scene 
                        key='checkout'
                        component={Checkout}
                        title='Checkout'
                    />
                </Scene>
            </Scene>
        </Router>
    );
};

const styles = {
    titleStyle: {
        flex: 1,
        textAlign: 'center'
    }
}

export default RouterComponent;
