import React, { Component } from 'react';
import firebase from 'firebase';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';

import ReduxThunk from 'redux-thunk';


import reducers from './reducers';
import Router from './Router';

class App extends Component {

  componentWillMount() {
    const config = {
      apiKey: 'AIzaSyA987btlwyI4QsG6Dmlg9Q64JsEGR1zlgw',
      authDomain: 'gamestop-6f040.firebaseapp.com',
      databaseURL: 'https://gamestop-6f040.firebaseio.com',
      projectId: 'gamestop-6f040',
      storageBucket: 'gamestop-6f040.appspot.com',
      messagingSenderId: '857593007512'
    };

    firebase.initializeApp(config);
  }
  
  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

    return (
      <Provider store={store}>
          <Router />
      </Provider>
      
    );
  }
}

export default App;
